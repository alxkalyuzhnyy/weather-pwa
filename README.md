# Weather PWA (Test)
This is a test progressive web app I've made to explain what's service workers and how it works.

This can be used as PWA starter kit as it features js unit tests and scss out of the box.

Weather info is taken from yahoo api. 

#### To get started:
* `npm install`
* `npm start`
